#!/bin/bash

echo "Creating images.. BackEnd and DataBase ..."

docker build -t robertoteixeira/projeto-dio-k8s-backend:1.0 backend/.
docker build -t robertoteixeira/projeto-dio-k8s-database:1.0 database/.

echo "Pushing the images to DockerHub ..."

docker push robertoteixeira/projeto-dio-k8s-backend:1.0
docker push robertoteixeira/projeto-dio-k8s-database:1.0

echo "Creating Kubernetes Cluster services ..."

kubectl apply -f ./services.yml

echo "Creating deployments ..."

kubectl apply -f ./deployment.yml
